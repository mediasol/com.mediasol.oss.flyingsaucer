# README #

This project takes Flying Saucer PDF generating library and spins it up as a standalone service that is able to convert XHTML input to PDF using REST calls.

### How do I get set up? ###

This is a pretty straight forward Maven Spring Boot project. You should be able to set it up without problems. If you run into troubles, raise an issue.
Check out com.mediasol.oss.flyingsaucer.fonts.rest.FontsRestController and com.mediasol.oss.flyingsaucer.pdf.rest.PDFRestController to get the idea on how to use the project.
This project uses MongoDB to store fonts that are loaded for the PDF generator.

### Contribution guidelines ###

You are welcome to contribute to the project. Please let us know on the email below.

### Who do I talk to? ###

In case of any questions, requests or suggestions, please contact us on lukas.zaruba@media-sol.com

### What can I do with this project ###

Well it is up to you, just mind the license. We do use it as an alternative service for PDF generation.

### License and Dependencies ###

Flying Saucer can use nice iText 5 library to produce PDFs. iText 5 is licensed under AGPL license. This license requires all dependant work to be realeased under the same license.
For our convenience this projects uses two simple libraries of our own that are not released under AGPL. These are com.mediasol.micro.common.mongo (common converters, mongo access and mongo test support) and com.mediasol.micro.oauth2.resource (JWT and authentication handling). You should be able to run this project just fine without given libraries (just remove them from pom.xml). If you'd really like to take a peak on how we configure things, let us know (on the email below) and we will be happy to provide you with source of these two libraries. Same applies for com.mediasol.micro.common.parent. Just replace it with common Spring Boot 2 parent.

** Currently some of the source code files contain header claiming that you need to obtain license from MEDIA SOLUTIONS. These headers made it to the source by mistake in the Eclipse configuration. We will do our best to replace these by AGPL headers ASAP. This note will apply even in the future as wrong headers will be kept in history. Thank you for you understanding. **
