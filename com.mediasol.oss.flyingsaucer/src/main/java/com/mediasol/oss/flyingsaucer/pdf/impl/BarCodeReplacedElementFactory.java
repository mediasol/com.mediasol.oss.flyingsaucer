/*
 * Copyright (C) 2018 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 */
package com.mediasol.oss.flyingsaucer.pdf.impl;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import org.springframework.data.util.Pair;
import org.w3c.dom.Element;
import org.xhtmlrenderer.extend.FSImage;
import org.xhtmlrenderer.extend.ReplacedElement;
import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.layout.LayoutContext;
import org.xhtmlrenderer.pdf.ITextFSImage;
import org.xhtmlrenderer.pdf.ITextImageElement;
import org.xhtmlrenderer.pdf.ITextOutputDevice;
import org.xhtmlrenderer.pdf.ITextReplacedElementFactory;
import org.xhtmlrenderer.render.BlockBox;

import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.Barcode;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.Barcode39;
import com.itextpdf.text.pdf.BarcodeCodabar;
import com.itextpdf.text.pdf.BarcodeEAN;
import com.itextpdf.text.pdf.BarcodeEANSUPP;
import com.itextpdf.text.pdf.BarcodeInter25;
import com.itextpdf.text.pdf.BarcodePostnet;

/**
 * @author Lukas Zaruba, lukas.zaruba@media-sol.com, MEDIA SOLUTIONS
 */
public class BarCodeReplacedElementFactory extends ITextReplacedElementFactory {

	private Map<String, Class<? extends Barcode>> barcodes = Map.of(
			"barcode128", Barcode128.class,
			"barcode39", Barcode39.class,
			"barcodeCodabar", BarcodeCodabar.class,
			"barcodeEAN", BarcodeEAN.class,
			"barcodeEANSUPP", BarcodeEANSUPP.class,
			"barcodeInter25", BarcodeInter25.class,
			"barcodePostnet", BarcodePostnet.class
	);
	public BarCodeReplacedElementFactory(ITextOutputDevice outputDevice) {
		super(outputDevice);
	}

	private Barcode getBarcode(String type) {
		Class<? extends Barcode> klass = barcodes.get(type);
		if (klass == null) {
			throw new IllegalArgumentException("Unknown barcode type " + type + ", supported types: " + barcodes.keySet());
		}
		try {
			return klass.getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			throw new RuntimeException("Error while instantiating " + klass, e);
		}
	}

	@Override
	public ReplacedElement createReplacedElement(LayoutContext c, BlockBox box,
			UserAgentCallback uac, int cssWidth, int cssHeight) {

		Element e = box.getElement();
		if (e == null) {
			return null;
		}

		String nodeName = e.getNodeName();
		if (nodeName.equals("img")) {
			String type = e.getAttribute("type");
			if (type != null && type.startsWith("barcode")) {
				try {
					Barcode code = getBarcode(type);
					code.setCode(e.getAttribute("src"));
					FSImage fsImage = new ITextFSImage(Image.getInstance(
							code.createAwtImage(Color.BLACK, Color.WHITE),
							Color.WHITE));
					if (cssWidth != -1 || cssHeight != -1) {
						fsImage.scale(cssWidth, cssHeight);
					}
					return new ITextImageElement(fsImage);
				} catch (Throwable e1) {
					return null;
				}
			} else {
				FSImage fsImage = uac.getImageResource(e.getAttribute("src"))
						.getImage();
				if (fsImage != null) {
					if (cssWidth != -1 || cssHeight != -1) {
						Pair<Integer, Integer> newSize = computeResizeBox(
								cssWidth, cssHeight, fsImage);
						if (newSize != null) {
							fsImage.scale(newSize.getFirst(),
									newSize.getSecond());
						}
					}
					return new ITextImageElement(fsImage);
				}
			}
		}

		return super.createReplacedElement(c, box, uac, cssWidth, cssHeight);
	}

	private Pair<Integer, Integer> computeResizeBox(int cssWidth,
			int cssHeight, FSImage fsImage) {
		if (cssWidth == -1 && cssHeight == -1) {
			return null;
		}

		int newWidth = -1;
		int newHeight = fsImage.getHeight();

		// Downsize an maintain aspect ratio...
		if (fsImage.getWidth() > cssWidth && cssWidth > -1) {
			newWidth = cssWidth;
			newHeight = (newWidth * fsImage.getHeight()) / fsImage.getWidth();
		}

		if (cssHeight > -1 && newHeight > cssHeight) {
			newHeight = cssHeight;
			newWidth = (newHeight * fsImage.getWidth()) / fsImage.getHeight();
		}

		// No resize required
		if (newWidth == -1) {
			return null;
		}

		// No upscaling!
		if (newWidth > fsImage.getWidth() || newWidth > fsImage.getHeight()) {
			return null;
		}

		return Pair.of(newWidth, newHeight);
	}

}
