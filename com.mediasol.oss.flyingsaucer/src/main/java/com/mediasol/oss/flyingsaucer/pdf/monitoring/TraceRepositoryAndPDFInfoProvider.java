/*
 * Copyright (C) 2023 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 */
package com.mediasol.oss.flyingsaucer.pdf.monitoring;

import org.springframework.boot.actuate.web.exchanges.HttpExchange;
import org.springframework.boot.actuate.web.exchanges.InMemoryHttpExchangeRepository;
import org.springframework.stereotype.Component;

import java.time.Duration;

/**
 * @author Jiří Škoda, jiri.skoda@media-sol.com, MEDIA SOLUTIONS
 */
@Component
public class TraceRepositoryAndPDFInfoProvider extends InMemoryHttpExchangeRepository {
	
	private long successfulOperations;
	private long errors;
	private Duration maxExecutionTime = Duration.ZERO;
	private Duration averageExecutionTime = Duration.ZERO;
	
	/**
	 * Adds trace to the repository and also collects summary information about generating PDF files.
	 */
	@Override
	public void add(HttpExchange trace) {
		super.add(trace);
		if (!trace.getRequest().getUri().getPath().equals("/api/v1/pdf")) return;
		if (trace.getResponse().getStatus() == 200) {
			Duration timeTaken = trace.getTimeTaken();
			if (timeTaken.compareTo(maxExecutionTime) > 0) maxExecutionTime = timeTaken;
			long sum = averageExecutionTime.toNanos() * successfulOperations + timeTaken.toNanos();
			successfulOperations++;
			averageExecutionTime = Duration.ofNanos(sum / successfulOperations);
		} else {
			errors++;
		}
	}
	
	/**
	 * Returns number of successful operations regarding generating PDF files
	 */
	public long getSuccessfulOperations() {
		return successfulOperations;
	}

	/**
	 * Returns number of error operations regarding generating PDF files
	 */
	public long getErrors() {
		return errors;
	}

	/**
	 * Returns max time taken to generate one PDF file
	 */
	public Duration getMaxExecutionTime() {
		return maxExecutionTime;
	}

	/**
	 * Returns average time taken to generate one PDF file
	 */
	public Duration getAverageExecutionTime() {
		return averageExecutionTime;
	}
	
}
