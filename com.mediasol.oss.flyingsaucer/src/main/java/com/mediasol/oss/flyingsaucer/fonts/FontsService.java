/*
 * Copyright (C) 2023 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 */
package com.mediasol.oss.flyingsaucer.fonts;

import com.mediasol.oss.flyingsaucer.fonts.model.Font;
import com.mediasol.oss.flyingsaucer.fonts.model.ResourceWrapper;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.io.InputStream;
import java.util.Collection;

/**
 * @author Jiří Škoda, jiri.skoda@media-sol.com, MEDIA SOLUTIONS
 */
public interface FontsService {
	
	static final String TTF_SUFFIX = ".ttf";
	static final String OTF_SUFFIX = ".otf";
	
	/**
	 * Inserts the font to the database.
	 */
	void saveFont(@NotNull InputStream file, @NotNull Font font);
	
	/**
	 * Deletes the font based on the ID from the database.
	 */
	void deleteFont(@NotEmpty String id);
	
	/**
	 * Returns all fonts (without content) from database.
	 */
	Collection<Font> listFonts();
	
	/**
	 * Returns the font (with its content) based on the ID form database.
	 */
	ResourceWrapper getFont(@NotEmpty String id);

	/**
	 * Returns all fonts (with their content) form the database.
	 */
	Collection<ResourceWrapper> getFonts();

}
