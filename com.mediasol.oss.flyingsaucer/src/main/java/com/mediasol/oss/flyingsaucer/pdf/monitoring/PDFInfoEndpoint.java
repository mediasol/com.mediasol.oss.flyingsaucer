/*
 * Copyright (C) 2019 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 */
package com.mediasol.oss.flyingsaucer.pdf.monitoring;

import com.mediasol.micro.common.exceptions.BadRequestException;
import com.mediasol.oss.flyingsaucer.config.PDFThreadConfigProperties;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import java.lang.Thread.State;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Jiří Škoda, jiri.skoda@media-sol.com, MEDIA SOLUTIONS
 */
@Component
@Endpoint(id = "pdf-info")
public class PDFInfoEndpoint {
	
	private static final String PARAM = "id";
	private static final String THREADS = "threads";
	private static final String ENDPOINT = "/actuator/pdf-info/" + THREADS;
	
	@Autowired
	private PDFThreadConfigProperties pdfProperties;
	
	@Autowired
	private TraceRepositoryAndPDFInfoProvider info;
 
	/**
	 * Returns summary information regarding generating PDF files.
	 */
    @ReadOperation
    public Map<String, Object> getSummaryInfo() {
    	Map<String, Object> map = new HashMap<>();
    	
    	Map<String, Object> propertiesMap = new HashMap<>();
    	propertiesMap.put("Max thread pool size", pdfProperties.getMaxPoolSize());
    	propertiesMap.put("Core thread pool size", pdfProperties.getCorePoolSize());
    	propertiesMap.put("Queue capacity", pdfProperties.getQueueCapacity());
    	propertiesMap.put("Timeout (min)", pdfProperties.getTimeoutMins());    	
    	map.put("Properties", propertiesMap);
    	
    	Map<String, Object> summaryMap = new HashMap<>();
    	summaryMap.put("Successful operations", info.getSuccessfulOperations());
    	summaryMap.put("Errors", info.getErrors());
    	summaryMap.put("Average time taken", info.getAverageExecutionTime());
    	summaryMap.put("Max time taken", info.getMaxExecutionTime());
    	map.put("Information", summaryMap);
    	
    	map.put("Active threads", Thread.getAllStackTraces().keySet().stream().filter(t -> t.getName().startsWith(pdfProperties.getThreadPrefix()))
    			.collect(Collectors.toMap(Thread::getName, Thread::getState)));
    	
    	map.put("Available endpoints", Map.of("Threads details", ENDPOINT));
    	
    	return map;
    }
    
    /**
     * Returns detailed info. Currently supports only details about threads for generating PDF files.
     */
	@ReadOperation
	public Object getInfo(@Selector String endpoint, @Nullable @RequestParam(PARAM) String id) {
    	if (endpoint.equals(THREADS)) {
    		if (StringUtils.isEmpty(id))
    			return Thread.getAllStackTraces().keySet().stream().filter(t -> t.getName().startsWith(pdfProperties.getThreadPrefix()))
    					.map(ThreadInfo::new).collect(Collectors.toList());
    		return Thread.getAllStackTraces().keySet().stream().filter(t -> t.getName().equals(pdfProperties.getThreadPrefix() + id))
    				.map(ThreadInfo::new).findFirst().orElseThrow(() -> new BadRequestException("Thread '" + id + "' does not exist"));
    	}
    	throw new BadRequestException("Endpoint '" + endpoint + "' does not exist");
    }
    
    /**
     * Private helper class that represents detail of one thread.
     */
    @Getter @Setter
    private class ThreadInfo {
    	
    	public ThreadInfo(Thread thread) {
    		name = thread.getName();
    		state = thread.getState();
    		ThreadGroup group = thread.getThreadGroup();
    		threadGroup = Map.of("parent", group.getParent().getName(), "activeThreads", group.activeCount(), "name", group.getName());
    		isAlive = thread.isAlive();
    		isInterrupted = thread.isInterrupted();
    		isDaemon = thread.isDaemon();
    		endpoint = ENDPOINT + "?" + PARAM + "=" + StringUtils.substringAfter(thread.getName(), pdfProperties.getThreadPrefix());
    	}
    	
    	private String name;
    	private State state;
    	private Map<String, Object> threadGroup;
    	private boolean isAlive;
    	private boolean isInterrupted;
    	private boolean isDaemon;
    	private String endpoint;
    	
    }
    
}
