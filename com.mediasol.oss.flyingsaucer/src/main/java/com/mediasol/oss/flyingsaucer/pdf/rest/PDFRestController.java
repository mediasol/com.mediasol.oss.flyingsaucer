/*
 * Copyright (C) 2023 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 */
package com.mediasol.oss.flyingsaucer.pdf.rest;

import com.mediasol.micro.common.logging.HasLogger;
import com.mediasol.oss.flyingsaucer.config.PDFThreadConfigProperties;
import com.mediasol.oss.flyingsaucer.pdf.PDFGenerator;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author Jiří Škoda, jiri.skoda@media-sol.com, MEDIA SOLUTIONS
 */
@RestController
@RequestMapping("/api/v1/pdf")
public class PDFRestController implements HasLogger {

	@Autowired
	private PDFGenerator generator;

	@Autowired
	private PDFThreadConfigProperties properties;

	@Autowired
	private TaskExecutor executor;

	/**
	 * Generates PDF file from the XML file using all available fonts. This method uses a thread
	 * from a thread pool and run it asynchronously to complete the task. If there are no available
	 * threads, task is queued until one of the threads is released or specified timeout expires.
	 */
	@PostMapping
	public void generatePDF(@RequestBody String xml, HttpServletResponse response)
			throws InterruptedException, ExecutionException, TimeoutException {
		CompletableFuture.runAsync(() -> {
			response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=file.pdf");
			response.setContentType(PDFGenerator.PDF_MIME_TYPE);
			try (OutputStream os = response.getOutputStream()) {
				generator.generatePDF(xml, os);
			} catch (IOException e) {
				getLogger().error("Error occurred while writing to stream", e);
				throw new RuntimeException(e);
			}
		}, executor).get(properties.getTimeoutMins(), TimeUnit.MINUTES);
	}

}
