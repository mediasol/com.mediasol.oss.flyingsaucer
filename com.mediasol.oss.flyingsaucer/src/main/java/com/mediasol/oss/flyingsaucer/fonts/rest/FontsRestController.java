/*
 * Copyright (C) 2023 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 */
package com.mediasol.oss.flyingsaucer.fonts.rest;

import com.mediasol.oss.flyingsaucer.fonts.FontsService;
import com.mediasol.oss.flyingsaucer.fonts.model.Font;
import com.mediasol.oss.flyingsaucer.fonts.model.ResourceWrapper;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

/**
 * @author Jiří Škoda, jiri.skoda@media-sol.com, MEDIA SOLUTIONS
 */
@RestController
@RequestMapping("/api/v1/fonts")
public class FontsRestController {

	@Autowired
	private FontsService service;

	/**
	 * Saves font based on given binary file and URL parameters.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@PutMapping
	public void saveFont(HttpServletRequest request, @RequestParam String filename, @RequestParam(defaultValue = "Identity-H") String encoding,
			@RequestParam(defaultValue = "false") boolean embedd) throws IOException {
		try (ServletInputStream is = request.getInputStream()) {
			service.saveFont(is, new Font(filename, encoding, embedd));
		}
	}

	/**
	 * Deletes the font based on the ID.
	 */
	@DeleteMapping("/{id}")
	public void deleteFont(@PathVariable String id) {
		service.deleteFont(id);
	}

	/**
	 * Returns the font based on the ID.
	 */
	@GetMapping("/{id}")
	public void getFontContent(HttpServletResponse response, @PathVariable String id) throws IOException {
		ResourceWrapper file = service.getFont(id);
		if (file == null) return;
		response.addHeader("Content-disposition", "attachment;filename=" + file.getFont().getFilename());
		try (InputStream is = file.getResource().getInputStream();
				ServletOutputStream os = response.getOutputStream()) {
			IOUtils.copy(is, os);
		}
	}

	/**
	 * Returns all available fonts.
	 */
	@GetMapping
	public Collection<Font> listFonts() {
		return service.listFonts();
	}

}
