/*
 * Copyright (C) 2022 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 */
package com.mediasol.oss.flyingsaucer.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.task.DelegatingSecurityContextTaskExecutor;

/**
 * @author Jiří Škoda, jiri.skoda@media-sol.com, MEDIA SOLUTIONS
 */
@EnableAsync
@Configuration
@DependsOn("PDFThreadConfigProperties")
public class ThreadPoolConfig {
	
	@Autowired
	private PDFThreadConfigProperties pdfProperties;
	
	@Bean
    public TaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(pdfProperties.getCorePoolSize());
        executor.setMaxPoolSize(pdfProperties.getMaxPoolSize());
        executor.setQueueCapacity(pdfProperties.getQueueCapacity());
        executor.setThreadGroup(new ThreadGroup(pdfProperties.getThreadGroupName()));
        executor.setThreadNamePrefix(pdfProperties.getThreadPrefix());
        executor.afterPropertiesSet();
		return new DelegatingSecurityContextTaskExecutor(executor);
    }
	
	@Bean
	public MethodInvokingFactoryBean methodInvokingFactoryBean() {
	    MethodInvokingFactoryBean factoryBean = new MethodInvokingFactoryBean();
	    factoryBean.setTargetClass(SecurityContextHolder.class);
	    factoryBean.setTargetMethod("setStrategyName");
	    factoryBean.setArguments(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
	    return factoryBean;
	}
	
}
