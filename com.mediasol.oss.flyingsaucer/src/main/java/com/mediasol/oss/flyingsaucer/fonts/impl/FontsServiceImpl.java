/*
 * Copyright (C) 2023 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 */
package com.mediasol.oss.flyingsaucer.fonts.impl;

import com.mediasol.micro.common.logging.HasLogger;
import com.mediasol.oss.flyingsaucer.fonts.FontsService;
import com.mediasol.oss.flyingsaucer.fonts.model.Font;
import com.mediasol.oss.flyingsaucer.fonts.model.ResourceWrapper;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.model.GridFSFile;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Jiří Škoda, jiri.skoda@media-sol.com, MEDIA SOLUTIONS
 */
@Service
@Validated
public class FontsServiceImpl implements FontsService, HasLogger {
	
	@Autowired
	private GridFsTemplate gridTemplate;	
	private GridFSBucket gridBucket;
	
	@Autowired
	public FontsServiceImpl(MongoTemplate mongoTemplate) {
		gridBucket = GridFSBuckets.create(mongoTemplate.getDb());
	}
	
	@Override
	public void saveFont(@NotNull InputStream is, @NotNull Font font) {
		gridTemplate.store(is, font.getFilename(), font);
	}

	@Override
	public void deleteFont(@NotEmpty String id) {
		gridTemplate.delete(new Query(Criteria.where("_id").is(id)));
	}

	@Override
	public Collection<Font> listFonts() {
		return gridTemplate.find(new Query()).map(this::convertToFont).into(new ArrayList<>());
	}

	@Override
	public ResourceWrapper getFont(@NotEmpty String id) {
		GridFSFile file = gridTemplate.findOne(new Query(Criteria.where("_id").is(id)));
		if (file == null) return null;
		return getResourceWrapper(file);
	}
	
	@Override
	public Collection<ResourceWrapper> getFonts() {
		return gridTemplate.find(new Query()).map(this::getResourceWrapper).into(new ArrayList<>());
	}
	
	private Font convertToFont(@NotNull GridFSFile dbFile) {
		Document metadata = dbFile.getMetadata();
		return new Font(dbFile.getObjectId().toString(), metadata.getString("filename"), metadata.getString("encoding"), metadata.getBoolean("embedd"));
	}
	
	private ResourceWrapper getResourceWrapper(@NotNull GridFSFile file) {
		return new ResourceWrapper(new GridFsResource(file, gridBucket.openDownloadStream(file.getObjectId())), convertToFont(file));
	}
	
}
