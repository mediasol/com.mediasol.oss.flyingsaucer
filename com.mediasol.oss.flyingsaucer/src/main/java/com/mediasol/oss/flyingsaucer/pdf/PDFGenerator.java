/*
 * Copyright (C) 2023 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 */
package com.mediasol.oss.flyingsaucer.pdf;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.io.OutputStream;

/**
 * @author Jiří Škoda, jiri.skoda@media-sol.com, MEDIA SOLUTIONS
 */
public interface PDFGenerator {
	
	static final int DPI_CONVERSION = 72;
	static final String PDF_MIME_TYPE = "application/pdf";
	static final float DPI_300_DOTS_PER_POINT = 300f / DPI_CONVERSION;
	static final float DPI_96_DOTS_PER_POINT = 96f / DPI_CONVERSION;

	/**
	 * Generates PDF file from the XML file using all available fonts and given DPI.
	 */
	void generatePDF(@NotEmpty String xml, @NotNull OutputStream os, float dpi);
	
	/**
	 * Generates PDF file from the XML file using all available fonts and default DPI.
	 */
	void generatePDF(@NotEmpty String xml, @NotNull OutputStream os);

}
