/*
 * Copyright (C) 2023 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 */
package com.mediasol.oss.flyingsaucer.pdf.impl;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.CMapAwareDocumentFont;
import com.itextpdf.text.pdf.PdfDictionary;
import com.mediasol.micro.common.logging.HasLogger;
import com.mediasol.oss.flyingsaucer.fonts.FontsService;
import com.mediasol.oss.flyingsaucer.fonts.model.Font;
import com.mediasol.oss.flyingsaucer.pdf.PDFGenerator;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.xhtmlrenderer.fop.FOPLineBreakingStrategy;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.pdf.ITextUserAgent;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Jiří Škoda, jiri.skoda@media-sol.com, MEDIA SOLUTIONS
 */
@Service
@Validated
@Slf4j
public class PDFGeneratorImpl implements PDFGenerator, HasLogger {

	/**
	 * @author Lukas Zaruba, lukas.zaruba@media-sol.com, MEDIA SOLUTIONS
	 *
	 * Added to access font cache which is hard wired into the itext
	 */
	private static class FakeFont extends CMapAwareDocumentFont {

		public FakeFont(PdfDictionary font) {
			super(font);
		}

		public static void clearFontCache() {
			fontCache.clear();
		}

	}

	@Autowired
	private FontsService service;

	@Override
	public void generatePDF(@NotEmpty String xml, @NotNull OutputStream os, float dpi) {
		Set<Path> downloadedFonts = new HashSet<>();
		try {
			ITextRenderer renderer = new ITextRenderer(dpi, 1);
			service.getFonts().forEach(file -> {
				Font font = file.getFont();
				if (font.getFilename().endsWith(FontsService.TTF_SUFFIX) || font.getFilename().endsWith(FontsService.OTF_SUFFIX)) {
					try {
						Path temp = Files.createTempFile("temp", font.getFilename().substring(font.getFilename().lastIndexOf(".")));
						try (OutputStream output = Files.newOutputStream(temp);
								InputStream input = file.getResource().getInputStream()) {
							IOUtils.copy(input, output);
						}
						renderer.getFontResolver().addFont(temp.toString(), font.getEncoding(), font.isEmbedd());
						downloadedFonts.add(temp);
					} catch (DocumentException | IOException e) {
						getLogger().error("Error occurred while adding font to PDF file", e);
					}
				}
			});
			ITextUserAgent ua = new ITextUserAgent(renderer.getOutputDevice(), 1) {

				@Override
				protected InputStream resolveAndOpenStream(String uri) {
					throw new UnsupportedOperationException();
				}

				@Override
				public String resolveURI(String uri) {
					return uri;
				}

			};
			//ua.setSharedContext(renderer.getSharedContext());
			renderer.getSharedContext().setReplacedElementFactory(new BarCodeReplacedElementFactory(renderer.getOutputDevice()));
			renderer.getSharedContext().setUserAgentCallback(ua);
			renderer.getSharedContext().setLineBreakingStrategy(new FOPLineBreakingStrategy());
			renderer.setDocumentFromString(xml);
			renderer.layout();
			renderer.createPDF(os);

			/**
			 * Clearing font cache to prevent memory leak caused by new fonts created every time based on random temp file names
			 * Cannot really stick to one name as updates of fonts wouldn't be available
			 */
			FakeFont.clearFontCache();
		} catch (IOException | DocumentException e) {
			throw new RuntimeException("Error occurred while creating PDF file", e);
		} finally {
			downloadedFonts.forEach(p -> {
				try {
					Files.delete(p);
				} catch (IOException e) {
					log.error("Error while deleting temp font", e);
				}
			});
		}
	}

	@Override
	public void generatePDF(@NotEmpty String xml, @NotNull OutputStream os) {
		generatePDF(xml, os, DPI_300_DOTS_PER_POINT);
	}

}
