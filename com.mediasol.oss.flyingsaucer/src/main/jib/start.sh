#!/bin/bash
if [ ! -z ${RESOURCES_URL+x} ];
then
  cd /
  git clone $RESOURCES_URL resources
  cd resources || exit
  chmod a+x run.sh
  ./run.sh
fi
java $JAVA_OPTS $ADDITIONAL_OPTS -cp @/app/jib-classpath-file @/app/jib-main-class-file