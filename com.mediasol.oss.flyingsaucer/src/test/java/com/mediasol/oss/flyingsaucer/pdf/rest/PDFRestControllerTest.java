/*
 * Copyright (C) 2018 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 */
package com.mediasol.oss.flyingsaucer.pdf.rest;

import com.mediasol.micro.common.testing.restdocs.RestControllerJupiterTest;
import com.mediasol.oss.flyingsaucer.TestApplication;
import com.mediasol.oss.flyingsaucer.pdf.PDFGenerator;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;

import java.io.InputStream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Jiří Škoda, jiri.skoda@media-sol.com, MEDIA SOLUTIONS
 */
@ContextConfiguration(classes = TestApplication.class)
public class PDFRestControllerTest extends RestControllerJupiterTest {
	
	private static final String CTX = "/flyingsaucer";
	private static final String BASE = CTX + "/api/v1/pdf";

	@MockBean
	private PDFGenerator generator;

	public PDFRestControllerTest() {
		super("example.com");
	}
	
	@Test
	public void generatePFDTest() throws Exception {
		try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("hello_world.xhtml")) {
			String xml = IOUtils.toString(is);
			mockMvc.perform(post(BASE).contextPath(CTX).contentType(MediaType.APPLICATION_XHTML_XML).content(xml)).andExpect(status().isOk());
		}
	}

	@Override
	protected String getCtx() {
		return CTX;
	}
}
