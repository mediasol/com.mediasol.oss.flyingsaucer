/*
 * Copyright (C) 2018 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 */
package com.mediasol.oss.flyingsaucer.fonts.impl;

import com.mediasol.micro.common.mongo.testsupport.MongoJupiterTest;
import com.mediasol.oss.flyingsaucer.TestApplication;
import com.mediasol.oss.flyingsaucer.fonts.FontsService;
import com.mediasol.oss.flyingsaucer.fonts.model.Font;
import com.mediasol.oss.flyingsaucer.fonts.model.ResourceWrapper;
import com.mongodb.client.gridfs.model.GridFSFile;
import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.test.context.ContextConfiguration;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Jiří Škoda, jiri.skoda@media-sol.com, MEDIA SOLUTIONS
 */
@ContextConfiguration(classes = TestApplication.class)
public class FontsServiceImplDBTest extends MongoJupiterTest {
	
	private static final String FILE = "font.ttf";

	@Autowired
	protected GridFsTemplate grid;
	
	@Autowired
	private FontsService service;
	
	private Font font;
	private InputStream is;
	
	@BeforeEach
	public void setUp() {
		font = new Font("name", "encoding", true);
		is = Thread.currentThread().getContextClassLoader().getResourceAsStream(FILE);
	}
	
	@AfterEach
	public void clean() throws IOException {
		is.close();
	}
	
	@Test
	public void saveFont() throws Exception {
		service.saveFont(is, font);
		GridFSFile result = grid.findOne(new Query());
		assertThat(result, notNullValue());
		assertThat(result.getMetadata().getString("filename"), equalTo("name"));
		assertThat(template.count(new Query(), "fs.chunks"), equalTo(1l));
		assertThat(template.count(new Query(), "fs.files"), equalTo(1l));
	}
	
	@Test
	public void deleteFont() throws Exception {
		ObjectId id = grid.store(is, "name", font);
		assertThat(template.count(new Query(), "fs.chunks"), equalTo(1l));
		assertThat(template.count(new Query(), "fs.files"), equalTo(1l));
		service.deleteFont(id.toString());
		assertThat(template.count(new Query(), "fs.chunks"), equalTo(0l));
		assertThat(template.count(new Query(), "fs.files"), equalTo(0l));
	}
	
	@Test
	public void listFonts() throws Exception {
		Font font2 = new Font("name2", "encoding", true);
		grid.store(is, "name", font);
		ObjectId id = grid.store(is, "name2", font2);
		Collection<Font> result = service.listFonts();
		assertThat(result.size(), equalTo(2));
		assertThat(result.stream().anyMatch(f -> f.getFilename().equals("name")), equalTo(true));
		assertThat(result.stream().anyMatch(f -> f.getId().equals(id.toString())
				&& f.getFilename().equals("name2")), equalTo(true));
	}
	
	@Test
	public void getFont() throws Exception {
		ObjectId id = grid.store(is, "name", font);
		Font font2 = new Font("name2", "encoding", true);
		grid.store(Thread.currentThread().getContextClassLoader().getResourceAsStream("empty_font.ttf"), "name2", font2);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ResourceWrapper file = service.getFont(id.toString());
		try (InputStream is2 = file.getResource().getInputStream()) {
			IOUtils.copy(is2, os);
		}
		String result = os.toString();
		os.reset();
		try (InputStream is2 = Thread.currentThread().getContextClassLoader().getResourceAsStream(FILE)) {
			IOUtils.copy(is2, os);
		}
		assertThat(result, equalTo(os.toString()));
		assertThat(file.getFont().getFilename(), equalTo(font.getFilename()));
	}
	
	@Test
	public void getFonts() throws Exception {
		grid.store(is, "name", font);
		Font font2 = new Font("name2", "encoding", true);
		grid.store(Thread.currentThread().getContextClassLoader().getResourceAsStream("empty_font.ttf"), "name2", font2);
		Collection<ResourceWrapper> result = service.getFonts();
		assertThat(result.size(), equalTo(2));
		assertThat(result.stream().anyMatch(r -> r.getFont().getFilename().equals("name2")), equalTo(true));
		try (InputStream is = result.stream().findFirst().get().getResource().getInputStream();
				InputStream is2 = Thread.currentThread().getContextClassLoader().getResourceAsStream("font.ttf")) {
			assertThat(IOUtils.toString(is), equalTo(IOUtils.toString(is2)));
		}
	}
	
}
