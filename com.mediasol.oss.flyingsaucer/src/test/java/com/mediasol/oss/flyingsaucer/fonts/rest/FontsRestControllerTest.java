/*
 * Copyright (C) 2018 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 */
package com.mediasol.oss.flyingsaucer.fonts.rest;

import com.mediasol.micro.common.testing.restdocs.RestControllerJupiterTest;
import com.mediasol.oss.flyingsaucer.TestApplication;
import com.mediasol.oss.flyingsaucer.fonts.FontsService;
import com.mediasol.oss.flyingsaucer.fonts.model.Font;
import com.mediasol.oss.flyingsaucer.fonts.model.ResourceWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ContextConfiguration;

import java.io.InputStream;
import java.util.List;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Jiří Škoda, jiri.skoda@media-sol.com, MEDIA SOLUTIONS
 */
@ContextConfiguration(classes = TestApplication.class)
public class FontsRestControllerTest extends RestControllerJupiterTest {
	
	private static final String CTX = "/flyingsaucer";
	private static final String BASE = CTX + "/api/v1/fonts";

	@MockBean
	private FontsService service;

	public FontsRestControllerTest() {
		super("example.com");
	}
	
	@Test
	public void listFontsTest() throws Exception {
		List<Font> fonts = List.of(new Font("123", "name", "coding", true), new Font());
		when(service.listFonts()).thenReturn(fonts);
		mockMvc.perform(get(BASE).contextPath(CTX))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.[0].id", is("123")))
				.andExpect(jsonPath("$.[0].filename", is("name")))
				.andExpect(jsonPath("$.[1].id", nullValue()));
		verify(service, times(1)).listFonts();
	}

	@Test
	public void getFontContentTest() throws Exception {
		GridFsResource r = mock(GridFsResource.class);
		when(r.getInputStream()).thenReturn(Thread.currentThread().getContextClassLoader().getResourceAsStream("font.ttf"));
		when(service.getFont("123")).thenReturn(new ResourceWrapper(r, new Font("123", "file.ttf", "Unicode", true)));
		mockMvc.perform(get(BASE + "/123").contextPath(CTX))
				.andExpect(status().isOk())
				.andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=file.ttf"));
	}
	
	@Test
	public void saveFontOneParam() throws Exception {
		mockMvc.perform(put(BASE + "?filename=file.ttf").contextPath(CTX))
				.andExpect(status().isCreated());
		verify(service, times(1)).saveFont(any(InputStream.class), any(Font.class));
	}
	
	@Test
	public void saveFontAllParams() throws Exception {
		mockMvc.perform(put(BASE + "?filename=file.ttf&encoding=Unicode&embedd=true").contextPath(CTX))
				.andExpect(status().isCreated());
		verify(service, times(1)).saveFont(any(InputStream.class), any(Font.class));
	}
	
	@Test
	public void deleteFontTest() throws Exception {
		mockMvc.perform(delete(BASE + "/123").contextPath(CTX)).andExpect(status().isOk());
		verify(service, times(1)).deleteFont("123");
	}

	@Override
	protected String getCtx() {
		return CTX;
	}
}
