/*
 * Copyright (C) 2023 MEDIA SOLUTIONS
 * All Rights Reserved.  No use, copying or distribution of this
 * work may be made except in accordance with a valid license
 * agreement from MEDIA SOLUTIONS. This notice must be
 * included on all copies, modifications and derivatives of this
 * work.
 *
 * MEDIA SOLUTIONS MAKES NO REPRESENTATIONS OR WARRANTIES
 * ABOUT THE SUITABILITY OF THE SOFTWARE, EITHER EXPRESSED OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 * MEDIA SOLUTIONS SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 */
package com.mediasol.oss.flyingsaucer.pdf.impl;

import com.mediasol.micro.common.testing.configs.ValidationTestConfig;
import com.mediasol.micro.common.tests.ExcludeFromTests;
import com.mediasol.oss.flyingsaucer.fonts.FontsService;
import com.mediasol.oss.flyingsaucer.fonts.model.Font;
import com.mediasol.oss.flyingsaucer.fonts.model.ResourceWrapper;
import com.mediasol.oss.flyingsaucer.pdf.PDFGenerator;
import jakarta.validation.ConstraintViolationException;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Jiří Škoda, jiri.skoda@media-sol.com, MEDIA SOLUTIONS
 */
@ExtendWith(SpringExtension.class)
public class PDFGeneratorImplTest {

	@TestConfiguration
	@ExcludeFromTests
	@Import(ValidationTestConfig.class)
	static class TestConfig {

		@Bean
		public PDFGenerator pdfGenerator() {
			return new PDFGeneratorImpl();
		}

	}

	@Autowired
	private PDFGenerator pdfGenerator;

	@MockBean
	private FontsService fontsService;

	@Test
	public void generatePDF() throws Exception {
		GridFsResource r = mock(GridFsResource.class);
		when(r.getInputStream()).thenReturn(Thread.currentThread().getContextClassLoader().getResourceAsStream("font.ttf"));
		when(fontsService.getFonts()).thenReturn(List.of(new ResourceWrapper(r, new Font("123", "file.ttf", "Unicode", true))));
		File file = Files.createTempFile("test", ".pdf").toFile();
		try (FileOutputStream os = new FileOutputStream(file);
				InputStream rs = Thread.currentThread().getContextClassLoader().getResourceAsStream("hello_world.xhtml")) {
			pdfGenerator.generatePDF(IOUtils.toString(rs), os);
			try (InputStream is = new FileInputStream(file)) {
				assertThat(IOUtils.toString(is), containsString("Hello world"));
			}
		}
	}

	@Test
	public void generatePDFError() throws Exception {
		GridFsResource r = mock(GridFsResource.class);
		when(r.getInputStream()).thenReturn(Thread.currentThread().getContextClassLoader().getResourceAsStream("font.ttf"));
		when(fontsService.getFonts()).thenReturn(List.of(new ResourceWrapper(r, new Font("123", "file.ttf", "Unicode", true))));
		File file = Files.createTempFile("test", ".pdf").toFile();
		try (FileOutputStream os = new FileOutputStream(file);
				InputStream rs = Thread.currentThread().getContextClassLoader().getResourceAsStream("hello_world_error.xhtml")) {
			Assertions.assertThrows(RuntimeException.class, () -> pdfGenerator.generatePDF(IOUtils.toString(rs), os));
		}
	}

	@Test
	public void generatePDFNullInput() {
		Assertions.assertThrows(ConstraintViolationException.class, () -> pdfGenerator.generatePDF(null, null));
	}

}
